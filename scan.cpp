#include <cstddef>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <omp.h>
#include <immintrin.h>

using std::min;

// index arrays for merging and interleaving
__declspec(align(64),target(mic)) static const int even_merge_idx[16]
  = {0,2,4,6,8,10,12,14,0,2,4,6,8,10,12,14};
__declspec(align(64),target(mic)) static const int odd_merge_idx[16]
  = {1,3,5,7,9,11,13,15,1,3,5,7,9,11,13,15};
__declspec(align(64),target(mic)) static const int left_interleave_idx[16]
  = {0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7};
__declspec(align(64),target(mic)) static const int right_interleave_idx[16]
  = {8,8,9,9,10,10,11,11,12,12,13,13,14,14,15,15};

// zero array for loading
__declspec(align(64),target(mic)) static const int zeros[16]
  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// masks for mering and interleaving
__declspec(target(mic)) static const int merge_mask = 0xFF00;
__declspec(target(mic)) static const int interleave_mask = 0xAAAA;

__attribute__((target(mic))) inline bool
flag_at(int flag, int pos) {
  return flag & (1<<pos);
}

__attribute__((target(mic))) inline void
clear_bit(int& flag, int pos) {
  flag &= ~(1<<pos);
}

__attribute__((target(mic))) inline void
set_bit(int& flag, int value, int pos) {
  clear_bit(flag, pos);
  flag |= (!!(value) << pos);
}

void print_array(int input[], int intflags[], int n) {
  for (int i = 0; i < n; ++i)
    printf("%3d ", input[i]);
  printf("\n");
  for (int i = 0; i < n/16; ++i) {
    for (int j = 0; j < 16; ++j) {
      printf("%3d ", !!(intflags[i] & (1<<j)));
    }
  }
  printf("\n");
}

__attribute__((target(mic))) int*
xeon_phi_initial_flags(int* flags, int n) {
  int *initial_flags;
  posix_memalign((void**)&initial_flags, 64, n*sizeof(int)/16);
  // copy and perform bit shift by one to the right
  // abuse that int is 32 bit but we just use 16
  int last = 0;
  for (int i = 0; i < n/16; ++i){
    last |= flags[i];
    initial_flags[i] = (last >> 1) & 0xFF;
    last = last << 16;
  }
  return initial_flags;
}


/**
 * find ideal height of one block
 *
 * limitations:
 * 512 KB cache per core / 4 threads / sizeof(int)+2*sizeof(char) -> ~10k
 * array entries per thread -> d_max = 13 (at n = ~2e6)
 */
__attribute__((target(mic))) int
get_blockd(int n) {
  // limit at 13 due to cache sizes
  if (n >= 1966080)
    return 13;

  // else keep at least 4 levels to reduce SIMD
  return log2(n)-4;
}

void
xeon_phi_make_flags(char in[], int out[], int n) {
  for (int i = 0; i < n; i+=16) {
    out[i/16] = 0;
    for (int j = 0; j < 16; ++j) {
      out[i/16] |= (in[i+j] << j);
    }
  }
}

/**
 * performs the upscan of a segmented scan on 16 packed int
 */
__attribute__((target(mic))) void
xeon_phi_segmented_upscan_inner_interleaved(int input[], int flags[], int startd, int stopd) {
  const int stepsize = 512/8/sizeof(int); // 16
  for (int d = startd; d < stopd; ++d) {
    const int sep = (1 << d);

    const __m512i odd_idx = _mm512_load_epi32(odd_merge_idx);
    const __m512i even_idx = _mm512_load_epi32(even_merge_idx);
    const __mmask16 right_mask = _mm512_int2mask(merge_mask);

    for (int k = 0; k < (1<<stopd); k+=2*sep) {
#ifdef __MIC__
      // load two data arrays
      const __m512i left = _mm512_load_epi32(input + (k+sep-1)*stepsize);
      const __m512i right = _mm512_load_epi32(input + (k+2*sep-1)*stepsize);

      // merge them into two new
      __m512i even = _mm512_permutevar_epi32(even_idx, left);
      even = _mm512_mask_permutevar_epi32(even, right_mask, even_idx, right);
      __m512i odd = _mm512_permutevar_epi32(odd_idx, left);
      odd = _mm512_mask_permutevar_epi32(odd, right_mask, odd_idx, right);

      // merge flags
      const int left_flags = flags[k+sep-1];
      const int right_flags = flags[k+2*sep-1];

      int odd_flags = 0;
      int even_flags = 0;
      for (int i = 0; i < 8; ++i) {
        odd_flags |= ((1 << i) & (left_flags >> i));
        odd_flags |= ((1 << (i+8)) & (right_flags << 8-i));
        even_flags |= ((1 << i) & (left_flags >> i+1));
        even_flags |= ((1 << (i+8)) & (right_flags << 8-i+1));
      }

      // store and perform or between flags
      flags[k+sep-1] = even_flags;
      flags[k+2*sep-1] = even_flags | odd_flags;

      // load flag
      const int flags = ~odd_flags; // we must check on not flag
      const __mmask16 flags_mask = _mm512_int2mask(flags);


      // this command performs the if check & branch
      odd = _mm512_mask_add_epi32(odd, flags_mask, even, odd);

      // save the changed values
      _mm512_store_epi32(input + (k+sep-1)*stepsize, even);
      _mm512_store_epi32(input + (k+2*sep-1)*stepsize, odd);
#else // TODO
#endif
    }
  }
}

/**
 * compute 4 upmost levels of binary tree (final SIMD array)
 */
__attribute__((target(mic))) void
xeon_phi_segmented_upscan_final(int input[], int& flags) {
  // just upscan with d=4
  for (int d = 0; d < 4; ++d) {
    for (int k = 0; k < 16; k+=(2<<d)) {
      if (!flag_at(flags, k+(2<<d)-1)) {
        input[k+(2<<d)-1] += input[k+(1<<d)-1];
      }
      const int left  = flag_at(flags, k+(2<<d)-1);
      printf("%x %x\t",  k+(2<<d)-1, left);
      const int right = flag_at(flags, k+(1<<d)-1);
      printf("%x %x\t",  k+(1<<d)-1, right);
      set_bit(flags, left || right, k+(2<<d)-1);
      printf("%x\n", flags);
    }
  }
}

__attribute__((target(mic))) void
xeon_phi_segmented_downscan_inner_interleaved(int input[], int flags[],
                                              const int initial_flags[],
                                              int startd, int stopd) {
  const int stepsize = 512/8/sizeof(int); // 16
  for (int d = stopd-1; d >= startd; --d) {
    const int sep = (1 << d);

#ifdef __MIC__
    const __m512i left_idx = _mm512_load_epi32(left_interleave_idx);
    const __m512i right_idx = _mm512_load_epi32(right_interleave_idx);
    const __mmask16 mask = _mm512_int2mask(interleave_mask);
    const __m512i zero = _mm512_load_epi32(zeros);

    for (int k = 0; k < (1<<stopd); k+=2*sep) {
      // load values
      const __m512i even = _mm512_load_epi32(input + (k+sep-1)*stepsize);
      const __m512i odd = _mm512_load_epi32(input + (k+2*sep-1)*stepsize);

      // load masks
      const __mmask16 flag_mask_minus_one = _mm512_int2mask(~flags[k+sep-1]);
      // shift by one bit is done while initializing the flags
      const __mmask16 flag_mask_zero = _mm512_int2mask(initial_flags[k+sep-1]);

      __m512i tright;
      // assign right to left side
      const __m512i tleft = odd;

      // perform computation
      // start with the lower if
      tright = _mm512_mask_add_epi32(even, flag_mask_minus_one, even, odd);
      // set some values to zero (first if)
      tright = _mm512_mask_blend_epi32(flag_mask_zero, tright, zero);

      // revert permutation
      __m512i left = _mm512_permutevar_epi32(left_idx, tleft);
      left = _mm512_mask_permutevar_epi32(left, mask, left_idx, tright);
      __m512i right = _mm512_permutevar_epi32(right_idx, tleft);
      right = _mm512_mask_permutevar_epi32(right, mask, right_idx, tright);

      // unset flag (not necessary, would be overwritten)
//      flags[k+sep-1] = 0;

      // revert bitmask permutation (perform interleave)
      {
        int left = 0;
        int right = 0;
        for (int i = 0; i < 8; ++i) {
          // flags[k+sep-1] is unset in loop
          set_bit(left, 0, 2*i);
          set_bit(left, flag_at(flags[k+2*sep-1], i), 2*i+1);
          set_bit(right, 0, 2*i);
          set_bit(right, flag_at(flags[k+2*sep-1], i+8), 2*i+1);
        }
        flags[k+sep-1] = left;
        flags[k+2*sep-1] = right;
      }

      // save
      _mm512_store_epi32(input + (k+sep-1)*stepsize, left);
      _mm512_store_epi32(input + (k+2*sep-1)*stepsize, right);
    }
#else
  //TODO
#endif
  }
}

/**
 * compute 4 upmost levels of binary tree (final SIMD array)
 */
__attribute__((target(mic))) void
xeon_phi_segmented_downscan_final(int input[], int& flags, const int initial_flags) {
  // just downscan with d=4
  for (int d = 3; d >= 0; --d) {
    for (int k = 0; k < 16; k+=(2<<d)) {
      // 2^d = (1<<d)
      // 2^d+1 = (2<<d)

      const int temp = input[k+(1<<d)-1];
      input[k+(1<<d)-1] = input[k+(2<<d)-1];

      // -1 corrects for the shifted initial_flags array
      if (flag_at(initial_flags, k+(1<<d)-1)) {
        input[k+(2<<d)-1] = 0;
      } else if (flag_at(flags, k+(1<<d)-1)) {
        input[k+(2<<d)-1] = temp;
      } else {
        input[k+(2<<d)-1] += temp;
      }
      // clear bit
      clear_bit(flags, (k+(1<<d)-1));
    }
  }
}

__attribute__((target(mic))) inline void
xeon_phi_segmented_upscan_final_interleaved(int input[], int flags[], int n) {
  xeon_phi_segmented_upscan_final(input + n - 16, flags[n/16-1]);
}

__attribute__((target(mic))) void
xeon_phi_segmented_upscan_interleaved(int input[], int flags[], int n) {
  // d value up to which all loop computations are done by one block
  const int blockd = get_blockd(n);
  const int maxblockd = log2(n)-4;

# pragma omp parallel for
  for (int b = 0; b < n/16; b+=(1<<blockd)) {
    xeon_phi_segmented_upscan_inner_interleaved(input + b, flags + b, 0, blockd);
  }

  // still vectorized, but single threaded accumulation of upper tree
  // (should be parallel for large n)
  xeon_phi_segmented_upscan_inner_interleaved(input, flags, blockd, maxblockd);

  xeon_phi_segmented_upscan_final_interleaved(input, flags, n);
}

__attribute__((target(mic))) inline void
xeon_phi_segmented_downscan_final_interleaved(int input[], int flags[],
                                            const int initial_flags[], int n) {
  xeon_phi_segmented_downscan_final(input + n - 16, flags[n/16-1],
                                  initial_flags[n/16-1]);
}

__attribute__((target(mic))) void
xeon_phi_segmented_downscan_interleaved(int input[], int flags[],
                                        const int initial_flags[], int n) {
  const int blockd = get_blockd(n);
  const int maxblockd = log2(n)-4;

  input[n-1] = 0;

  xeon_phi_segmented_downscan_final_interleaved(input, flags, initial_flags, n);

  xeon_phi_segmented_downscan_inner_interleaved(input, flags, initial_flags,
                                                blockd, maxblockd);

# pragma omp parallel for
  for (int b = 0; b < n/16; b+=(1<<blockd)) {
    xeon_phi_segmented_downscan_inner_interleaved(input + b, flags + b,
                                                  initial_flags + b, 0, blockd);
  }
}

__attribute__((target(mic))) void
xeon_phi_segmented_scan(int input[], char flags[], int n) {
  char *flags_orig = (char*)malloc(n);
  // d value up to which all loop computations are done by one block
  const int blockd = get_blockd(n);

  printf("blockd: %d\n", blockd);
  printf("log2(n-1) %d\n", int(log2(n-1)));
  printf("elements per segment %d\n", (1<<blockd));
  printf("number of segments %d\n", n/(1<<blockd));

  {
    for (int i = 0; i < n; ++i) {
      flags_orig[i] = flags[i];
    }

# pragma omp parallel for
    for (int b = 0; b < n; b+=(1<<blockd)) {
      // this loop is run by one core
      for (int d = 0; d < blockd; ++d) {
        const int twod = (1 << d);
        for (int k = b; k < b+(1<<blockd); k+=twod*2) {
          if (! flags[k+twod*2-1]) {
            input[k+twod*2-1] = input[k+twod-1] + input[k+twod*2-1];
          }
          flags[k+twod*2-1] = flags[k+twod-1] | flags[k+twod*2-1];
        }
      }
    }

    // complete upper part on single core
    for (int d = blockd; d < log2(n-1); ++d) {
      const int twod = (1 << d);
      for (int k = 0; k < n; k+=twod*2) {
        if (! flags[k+twod*2-1]) {
          input[k+twod*2-1] = input[k+twod-1] + input[k+twod*2-1];
        }
        flags[k+twod*2-1] = flags[k+twod-1] | flags[k+twod*2-1];
      }
    }

    input[n-1] = 0;

// downscan

    // start with single threaded downscan
    for (int d = log2(n-1); d >= blockd; --d) {
      const int twod = (1 << d);
      for (int k = 0; k < n; k+=twod*2) {
        const int temp = input[k+twod-1];
        input[k+twod-1] = input[k+twod*2-1];
        if (flags_orig[k+twod]) {
          input[k+twod*2-1] = 0;
        } else if (flags[k+twod-1]) {
          input[k+twod*2-1] = temp;
        } else {
          input[k+twod*2-1] += temp;
        }
        flags[k+twod-1] = 0;
      }
    }

# pragma omp parallel for
    for (int b = 0; b < n; b+=(1<<blockd)) {
      for (int d = blockd-1; d >= 0; --d) {
        const int twod = (1 << d);
        for (int k = b; k < b+(1<<blockd); k+=twod*2) {
          const int temp = input[k+twod-1];
          input[k+twod-1] = input[k+twod*2-1];
          if (flags_orig[k+twod]) {
            input[k+twod*2-1] = 0;
          } else if (flags[k+twod-1]) {
            input[k+twod*2-1] = temp;
          } else {
            input[k+twod*2-1] += temp;
          }
          flags[k+twod-1] = 0;
        }
      }
    }
  }

  free(flags_orig);
}

void
simple_segmented_scan(int input[], char flags[], int n) {
  char *flags_orig = (char*)malloc(n);
  memcpy(flags_orig, flags, n*sizeof(char));

  for (int d = 0; d < log2(n-1); ++d) {
    const int twod = (1 << d);
    for (int k = 0; k < n; k+=twod*2) {
      if (! flags[k+twod*2-1]) {
        input[k+twod*2-1] = input[k+twod-1] + input[k+twod*2-1];
      }
      flags[k+twod*2-1] = flags[k+twod-1] | flags[k+twod*2-1];
    }
  }

  input[n-1] = 0;

  for (int d = log2(n-1); d >= 0; --d) {
    const int twod = (1 << d);
    for (int k = 0; k < n; k+=twod*2) {
      const int temp = input[k+twod-1];
      input[k+twod-1] = input[k+twod*2-1];
      if (flags_orig[k+twod]) {
        input[k+twod*2-1] = 0;
      } else if (flags[k+twod-1]) {
        input[k+twod*2-1] = temp;
      } else {
        input[k+twod*2-1] += temp;
      }
      flags[k+twod-1] = 0;
    }
  }

  free(flags_orig);
}

inline void
segmented_scan(int input[], char flags[], int n) {
#pragma offload target(mic) inout(input,flags:length(n))
  {
    xeon_phi_segmented_scan(input, flags, n);
  }
}

void test1() {
  const size_t n = (1<<10);
  int input[n];
  for (int i = 0; i < n; ++i)
    input[i] = rand() % (1<<6);
  char flags[n];
  for (int i = 0; i < n; ++i)
    flags[i] = (rand() % 4)/3;
//  print_array(input, flags, 16);
  segmented_scan(input, flags, n);
//  print_array(input, flags, 16);
}

void test2(int n) {
  int *input = (int*)malloc(sizeof(int)*n);
  char *flags = (char*)malloc(n);
  for (int i = 0; i < min(n,100000); ++i)
    input[i] = rand() % (1<<6);
  for (int i = 0; i < min(n,100000); ++i)
    flags[i] = (rand() % 4)/3;
  flags[0] = 1;
//  print_array(input, flags, 16);
  segmented_scan(input, flags, n);
//  print_array(input, flags, 16);
}

void test3() {
  const int n = 1<<6;
  int *input;
  posix_memalign((void**)&input, 64, n*sizeof(int));
  for (int i = 0; i < min(n,100000); ++i)
    input[i] = 1;
  char *flags = (char*)malloc(n);
  for (int i = 0; i < min(n,100000); ++i)
    flags[i] = ((i%16)/15);
//    flags[i] = ((i%8)+1)/8;
    //flags[i] = (rand() % 4)/3;

//int *intflags = (int*)malloc(n*sizeof(int)/16);
  int *intflags;
  posix_memalign((void**)&intflags, 64, n*sizeof(int)/16);
  xeon_phi_make_flags(flags, intflags, n);
  print_array(input, intflags, 32);
#pragma offload target(mic) inout(input:length(n)) inout(intflags:length(n/16))
  {
    int *initial_flags = xeon_phi_initial_flags(intflags, n);
    xeon_phi_segmented_upscan_interleaved(input, intflags, n);
    xeon_phi_segmented_downscan_interleaved(input, intflags, initial_flags, n);
  }
  print_array(input, intflags, 32);
}

void test4() {
  const int n = 16;
  __declspec(align(64)) int input[16] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
  __declspec(align(64)) char flags[16] = {1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0};
  int *intflags;
  posix_memalign((void**)&intflags, 64, n*sizeof(int)/16);
  xeon_phi_make_flags(flags, intflags, n);
  print_array(input, intflags, 16);
#pragma offload target(mic) inout(input) inout(intflags:length(n/16))
  {
    int *initial_flags = xeon_phi_initial_flags(intflags, n);
    xeon_phi_segmented_upscan_final_interleaved(input, intflags, n);
    input[n-1]=0;
    xeon_phi_segmented_downscan_final_interleaved(input, intflags, initial_flags, n);
  }
  print_array(input, intflags, 16);
}

void testbit() {
  int bit = 0xFFFF;
  set_bit(bit, 1, 4);
  printf("0x%4x\n", bit);
  set_bit(bit, 1, 8);
  printf("0x%4x\n", bit);
  set_bit(bit, 0, 4);
  printf("0x%4x\n", bit);
  printf("%x\n", flag_at(bit, 4));
  printf("%x\n", flag_at(bit, 8));
}

int main(int argc, char **argv) {
  // recommended size for xeon phi (60cores * 4threads)
  omp_set_num_threads(240);
  //test2(atoi(argv[1]));
  test3();
  return 0;
}
