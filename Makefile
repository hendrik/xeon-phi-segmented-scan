CXX=icc
CXXFLAGS=-O3 -openmp -ggdb
# -vec-report2

all: scan

run: scan
	. /opt/intel/bin/compilervars.sh intel64
	SINK_LD_LIBRARY_PATH=/opt/intel/composer_xe_2015.1.133/compiler/lib/mic/ \
		micnativeloadex $<

scan: scan.cpp
	. /opt/intel/bin/compilervars.sh intel64
	$(CXX) $(CXXFLAGS) -o $@ $^

.PHONY: scan
